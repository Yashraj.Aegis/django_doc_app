from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),  # Include Application url here in project
    path('admin/', admin.site.urls),
]