from django.db import models
from django.utils import timezone  # Get time zone that given in settings.py TIME_ZONE
import datetime  # Returns Date and Time


class Question(models.Model):
    question_text = models.CharField(max_length=200)  # In CharField max_length is required field
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now


'''
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
'''


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)  # When we put FK then on_delete field required
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)  # Return integer and default value required in IntegerField

    def __str__(self):
        return self.choice_text
