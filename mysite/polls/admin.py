from django.contrib import admin

from .models import Question, Choice  # Import models from models.py

# admin.site.register(Question)
admin.site.register(Choice)  # Register that models here for view all the things happen at client side


class ChoiceInline(admin.TabularInline):  # put extra fields here
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):  # manage admin site question
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')  # display list of given headers
    list_filter = ['pub_date']  # provide inbuilt filter for date
    search_fields = ['question_text']  # we can search question


admin.site.register(Question, QuestionAdmin)
